from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Jessica Pertiwi' # TODO Implement this
mhs_age = 1995

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(mhs_age)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    age = datetime.now().year
    return age-birth_year

    
